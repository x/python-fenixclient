==================
python-fenixclient
==================

The API client and shell utility for Fenix

This is the client for OpenStack Fenix. It implements the Python API and the
CLI for this project.

* Free software: Apache license
* Documentation: https://wiki.openstack.org/wiki/Fenix
* Source: https://opendev.org/x/python-fenixclient
* Bugs tracking and Blueprints: https://storyboard.openstack.org/#!/project/x/python-fenixclient
* How to contribute: https://docs.openstack.org/infra/manual/developers.html

--------

* TODO
